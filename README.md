# atcbot
## これは何
Misskeyインスタンス上で動作するbot（のようなもの）．[先駆者](https://xuzijian629.hatenablog.com/entry/2018/12/12/181350)がいらっしゃるので少なくとも二番煎じですが……．概要は次の通りです：
- その日のうちに[AtCoder](https://atcoder.jp/)の問題を1題以上ACしていないと，怒りのノートを飛ばしてきます．
- 週に一度，その週にACされた問題一覧を送信します．

## 要件
- Python3（パッケージの新規インストールは不要です）
- インターネット回線（AtCoder ProblemsのAPIを利用するため）
- cronまたはこれに類するもの（以下の記述はcronを前提とします）

## 使い方
### 流れ
1. `git clone https://gitlab.com/azumabashi/atcbot.git`
2. `cp settings-sample.json settings.json`
3. `cp schedule-sample.txt schedule.txt`
4. `settings.json`に設定を書き込みます．`schedule.txt`で起動時間とそのコマンドを管理します．
5. `crontab schedule.txt`
5. 指定時間になると指定した動作が実行されます．

### `settings.json`への設定
すべてjson形式で指定します．
- `URL`：APIがアクセスするためのドメインを指定してください．
- `HEADER`：アクセス時のヘッダです．
- `TARGET`：`{AtCoder Account}: {Misskey Account}`の順に指定してください．
- `TOKEN`：トークンです．

### `schedule.txt`への設定
基本はcronの設定に準じます．注意事項として：
- ファイルへのパスはフルパスにしてください．
- pythonファイル実行時の引数で機能を分岐させています．次の2パターンに対応しています．これら以外のものを渡した場合は何も実行されません．
  - `angry <message>`：`「<message>AtCoderやれ」`という文字列を含むノートを作成します（`<message>`の部分は渡した文字列に置換されます）．`<message>`の部分はoptionです．
  - `weekrecord`：一週間に解かれた問題一覧のノートを作成します．optionで渡すような文字列はありません．

## つくった人
[Twitter](https://twitter.com/azm_bashi)


