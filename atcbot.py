import requests
from datetime import datetime, timezone, timedelta
from time import mktime, sleep
from sys import argv
from collections import defaultdict
import json
from os import chdir, path

chdir(path.dirname(path.abspath(__file__)))
SETTINGS = json.load(open('settings.json', 'r'))
URL = SETTINGS["URL"]["URL"]
HEADER = SETTINGS["HEADER"]
SEND_TARGET = SETTINGS["TARGET"]
TOKEN = SETTINGS["TOKEN"]["TOKEN"]
LOWER_SCORE = int(SETTINGS["LOWER_SCORE"]["LOWER_SCORE"])
MAX_LENGTH = int(SETTINGS["MAX_LENGTH"]["MAX_LENGTH"]) - 5  # 「(続)」の分を減らす

def get_json(url):
    res = requests.get(url).json()
    sleep(2)
    return res


def create_note(msg, cw="", reply_id=""):
    data = {}
    data["i"] = TOKEN
    data["text"] = msg
    if reply_id:
        data["replyId"] = reply_id
    # data["visibility"] = "specified"
    if cw:
        data["cw"] = cw
    data = json.dumps(data)
    return requests.post(URL, headers=HEADER, data=data.encode("utf-8")).json()


def get_unix_time(week=0,day=0,hour=0,minute=0):
    now = datetime.now()
    now -= timedelta(weeks=week, days=day, hours=hour, minutes=minute)
    target_dt = datetime(now.year, now.month, now.day, 0, 0, 0)
    return int(target_dt.timestamp())


def get_all_problem_info():
    all_info = get_json("https://kenkoooo.com/atcoder/resources/merged-problems.json")
    res = {}
    require = ["point", "title"]
    for info in all_info:
        res[info["id"]] = {r: info[r] for r in require}
    return res


def is_got_angry(target, unix_time, problem_info):
    query = "https://kenkoooo.com/atcoder/atcoder-api/results?user=" + target
    json = get_json(query)
    rated_point_sum = 0
    accepted_count = 0
    for j in json:
        if j["result"] != "AC" or j["epoch_second"] < unix_time:
            continue
        accepted_count += 1
        if j["problem_id"] not in problem_info or j["point"] is None or problem_info[j["problem_id"]]["point"] is None:
            return 2
        rated_point_sum += int(problem_info[j["problem_id"]]["point"])
    if accepted_count == 0:
        return 0
    elif rated_point_sum < LOWER_SCORE:
        return 1
    return 2


def angry_message(target, prefix):
    return "{} {}AtCoderやれ".format(" @".join(target), prefix)


def angry():
    users_solved_nothing = [""]
    users_solved_little = [""]
    unix_time = get_unix_time()
    all_info = get_all_problem_info()
    for atcoder_username, misskey_username in SEND_TARGET.items():
        res = is_got_angry(atcoder_username, unix_time, all_info)
        if res == 0:
            users_solved_nothing.append(misskey_username)
        elif res == 1:
            users_solved_little.append(misskey_username)
    if users_solved_nothing == users_solved_little == [""]:
        create_note("りんご")
        return
    message_prefix = ""
    if len(argv) > 2:
        message_prefix = argv[2]
    message = []
    if len(users_solved_nothing) > 1:
        message.append(angry_message(users_solved_nothing, message_prefix))
    if len(users_solved_little) > 1:
        message.append(angry_message(users_solved_little, "もうちょっと"))
    for m in message:
        create_note(m[1:])


def send_week_record():
    week_past = get_unix_time(week=1)
    all_problems = get_json("https://kenkoooo.com/atcoder/resources/problems.json")
    message = [[""]]
    checked = {}
    msg_len = 0
    for atcoder_username, misskey_username in SEND_TARGET.items():
        user_title = "【{}】".format(misskey_username)
        if msg_len + 130 > MAX_LENGTH:
            message.append([""])
            msg_len = 0
            # 何も情報が入らずに名前だけ出て終わるのはまずいので，厳しめに見る
        message[-1].append(user_title)
        msg_len += len(user_title) + 1
        problem_info = get_json("https://kenkoooo.com/atcoder/atcoder-api/results?user=" + atcoder_username)
        is_accepted = False
        for p in problem_info:
            if p["epoch_second"] >= week_past and p["result"] == "AC":
                q_id = p["problem_id"]
                if q_id in checked:
                    if msg_len + len(checked[q_id]) > MAX_LENGTH:
                        msg_len = 0
                        message[-1].append("(続)")
                        message.append([""])
                    message[-1].append(checked[q_id])
                    msg_len += len(checked[q_id]) + 1
                    # 改行が入るため+1，以下同様
                else:
                    q_info = ""
                    for all_p in all_problems:
                        if all_p["id"] == q_id:
                            q_info = "?[{}](https://atcoder.jp/contests/{}/tasks/{})".format(all_p["title"], all_p["contest_id"], all_p["id"])
                            break
                    checked[q_id] = q_info
                    if msg_len + len(q_info) > MAX_LENGTH:
                        msg_len = 0
                        message[-1].append("(続)")
                        message.append([""])
                    message[-1].append(q_info)
                    is_accepted = True
                    msg_len += len(q_info) + 1
        if not is_accepted:
            # うえで厳しめに見たのでここは甘めにする
            msg_not_accepted = "1問もやってないよ"
            message[-1].append(msg_not_accepted)
            msg_len += len(msg_not_accepted) + 1
    divide = len(message)
    reply_id = ""
    for i in range(divide):
        send_message = "\n".join(message[i])
        res = create_note(send_message, cw="今週解かれた問題(Since " + str(datetime.fromtimestamp(week_past)).replace("00:00:00", "00:<flip>00</flip>:00") + ", " + str(i + 1) + "/" + str(divide) +  ")\n", reply_id=reply_id)
        reply_id = res["createdNote"]["id"]


def main():
    if len(argv) == 1:
        return
    elif argv[1] == "angry":
        angry()
    elif argv[1] == "weekrecord":
        send_week_record()


if __name__ == '__main__':
    main()

